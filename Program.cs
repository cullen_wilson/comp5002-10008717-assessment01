﻿using System;

namespace comp5002_10008717_assessment01
{
    class Program
    {
        static void Main(string[] args)
        {
            //Start the program with Clear();
            Console.Clear();
            //Declare variables
            //String "userName" "decisionAnswer"
            //Double "numberOne"
            var userName = "0";
            var decisionAsnwer = "0";
            var num1 = 0.00;
          
            //Print "Welcome to my shop"
            //Print "Please enter your name"
            Console.WriteLine("Welcome to my shop!\nPlease enter your name");
            //Read entered name and store to variable "userName"
            userName = Console.ReadLine();
            //Print " "Hello "userName" Please type in your 2 decimal place number"
            Console.WriteLine($"Hello {userName} Please type in your 2 decimal place number");
            //Read entered number and save to "num1"
            num1 = Double.Parse(Console.ReadLine());
            //Print "You have entered "num1" Would you like to enter another number? y/n:"
            Console.WriteLine($"You have entered {num1} Would you like to enter another number? y/n:");
            //Read answer y/n and save to "decisionAnswer"
            decisionAsnwer = Console.ReadLine();
            
            if (decisionAsnwer == "y" || decisionAsnwer  == "Y")
            {
                //If user answers y print "Please enter another number?"
                Console.WriteLine("Please enter another number");
                //Read entered number and add to "num1"
                num1 += Double.Parse(Console.ReadLine());
                //Print your total excluding GST is: "num1"
                //Print your total including GST is: "num1 * 1.15"
                //Print "Press any key to continue"
                Console.WriteLine($"Your total excluding GST is: {num1}\nYour total including GST is {num1*1.15}\nPress any key to continue");
                Console.ReadKey();

            }
            else
            {
                //If user answers n print "Your total excluding GST is "num1"
                //Print your total including GST is: "num1 * 1.15"
                //"Press any key to continue"
                Console.WriteLine($"Your total excluding GST is: {num1}\nYour total including GST is {num1*1.15}\nPress any key to continue");
                Console.ReadKey();
                
            }
         

                //Print "Thank you for shopping at my store "userName"
                Console.WriteLine($"Thankyou for shopping at my store {userName}");
                //End the program with blank line and instructions
                Console.ResetColor();
                Console.WriteLine();
                Console.WriteLine("Press <Enter> to quit the program");
                Console.ReadKey();

        }      
    }
}

           